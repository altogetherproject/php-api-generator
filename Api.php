<?php

/*
 * Author: Raphael Martin
 * Version: 2017-09-04 
 */

class Api {
## global config parts

    protected $homeUrl, $disabledMessage, $appPath, $defaultHeader, $configApi, $altogetherToolsPath;

    ## constructor

    function __construct($homeUrl, $configApi, $altogetherToolsPath) {
        $this->homeUrl = $homeUrl;
        $this->configApi = $configApi;
        $this->altogetherToolsPath = $altogetherToolsPath;
        $this->defaultHeader = "view/html-header.php";
        $this->disabledMessage = "admin has to enable app";
        $this->appPath = "apps";
    }

    function setDefaultHeader($path2file) {
        $this->defaultHeader = $path2file;
    }

    function setDisabledMessage($message) {
        $this->disabledMessage = $message;
    }

    function setAppPath($path) {
        $this->appPath = $path;
    }

    static function array2url($arr, $reqLevel) {
        return strtolower(implode("/", array_slice($arr, 0, $reqLevel)));
    }

    function homeUrl() {
        header("Location: " . $this->homeUrl);
        exit();
    }

    static private function allowCors() {
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');    // cache for 1 day
        }

        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        }
    }

    protected function disabledApp() {
        echo $this->disabledMessage;
        exit();
    }

    protected function loadContent($altogetherToolsPath, $appPath, $appStartFile, $request, $requestLevel, $jsonView = false) {
        
        $homeUrl = $this->homeUrl;
        
        if (isset($configApi[$request[$requestLevel]]["enableCors"]) && $configApi[$request[$requestLevel]]["enableCors"]) {
            self::allowCors();
        }
        if ($jsonView) {
            header('Content-Type: application/json');
        } else {
            echo "<html><head>";
            require_once $this->defaultHeader;
        }

        require_once "$appPath/$appStartFile";
        exit();
    }

    protected function apiFunctions($request, $requestLevel, $configApi, $altogetherToolsPath) {
        if (!$configApi[$request[$requestLevel]]["enable"]) {
            $this->disabledApp();
        }
        if (isset($configApi[$request[$requestLevel]]["jsonView"])) {
            $jsonView = $configApi[$request[$requestLevel]]["jsonView"];
        } else {
            $jsonView = false;
        }
        if ((count($request) - 1 == $requestLevel) || (isset($configApi[$request[$requestLevel]]["topLevel"]) && $configApi[$request[$requestLevel]]["topLevel"])) {
            if (isset($configApi[$request[$requestLevel]]["redirect"], $configApi[$request[$requestLevel]]["redirectUrl"]) && $configApi[$request[$requestLevel]]["redirect"]) {
                $url = $configApi[$request[$requestLevel]]["redirectUrl"];
                header("Location: $url");
                exit;
            } else {
                $this->loadContent($altogetherToolsPath, $configApi[$request[$requestLevel]]["path"], $configApi[$request[$requestLevel]]["startFile"], $request, $requestLevel, $jsonView);
            }
        }
    }

    protected function checkApi($request, $requestLevel, $configApi, $homeUrl, $appPath, $altogetherToolsPath) {
        if (array_key_exists($request[$requestLevel], $configApi)) {
            $this->apiFunctions($request, $requestLevel, $configApi, $altogetherToolsPath);
            $requestLevel++;
            if (isset($configApi[strtolower($request[$requestLevel - 1])]["subs"]) && array_key_exists($request[$requestLevel], $configApi[$request[$requestLevel - 1]]["subs"])) {
                $configApi = $configApi[$request[$requestLevel - 1]]["subs"];
                $this->checkApi($request, $requestLevel, $configApi, $homeUrl, $appPath, $altogetherToolsPath);
                exit();
            } else {
                $apiPath = self::array2url($request, $requestLevel);
                header("Location: $homeUrl/$apiPath");
                exit();
            }
        }
    }

    function check($request, $requestLevel = 0) {
        $request = explode("/", strtolower($request));
        $this->checkApi($request, $requestLevel, $this->configApi, $this->homeUrl, $this->appPath, $this->altogetherToolsPath);
        $this->homeUrl();
    }

}